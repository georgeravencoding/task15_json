package com.kolchak.model;

public class Account {
    private int accountId;
    private Depositor depositor;
    private Deposit deposit;

    public Account() {
    }

    public Account(int accountId, Depositor depositor, Deposit deposit) {
        this.accountId = accountId;
        this.depositor = depositor;
        this.deposit = deposit;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getAccountId() {
        return accountId;
    }

    public Depositor getDepositor() {
        return depositor;
    }

    public void setDepositor(Depositor depositor) {
        this.depositor = depositor;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                ", depositor=" + depositor +
                ", deposit=" + deposit +
                '}';
    }
}
