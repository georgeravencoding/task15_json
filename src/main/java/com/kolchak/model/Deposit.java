package com.kolchak.model;

public class Deposit {

    private String type;
    private int amount;
    private float profitability;
    private int timeConstraints;

    public Deposit() {
    }

    public Deposit(String depositType, int amount, float profitability, int timeConstraints) {
        this.type = depositType;
        this.amount = amount;
        this.profitability = profitability;
        this.timeConstraints = timeConstraints;
    }

    public String getDepositType() {
        return type;
    }

    public void setDepositType(String type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getProfitability() {
        return profitability;
    }

    public void setProfitability(float profitability) {
        this.profitability = profitability;
    }

    public int getTimeConstraints() {
        return timeConstraints;
    }

    public void setTimeConstraints(int timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "depositType='" + type + '\'' +
                ", amount=" + amount +
                ", profitability=" + profitability +
                ", timeConstraints=" + timeConstraints +
                '}';
    }
}
