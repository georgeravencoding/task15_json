package com.kolchak.model;

public class Depositor {
    private String name;

    public Depositor() {
    }

    public Depositor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Depositor{" +
                "name='" + name + '\'' +
                '}';
    }
}
